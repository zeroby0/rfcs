* Start date: 2019-Jul-13
* Target Version: 0.x
* Status: Pending
* Reference Issues: (Null)
* Implementation PR: (Null)

# Summary

Advertise with a high frequency initially and then switch to a slower frequency.

# Example

* Advertise every 20 ms for 30 seconds after power on. (757 uA)
* Then switch to advertising every 1022.5 ms. (22 uA)

Power measurements assume a 31 byte payload.

# Motivation

Advertising interval of the Peripheral affects the time of discovery and connection.
A slower interval can save power but also makes the time to discovery longer.
The User is most likely to connect to the Peripheral shortly after switching it on.
So we can maintain the best UX by advertising rapidly at the beginning and slowing
down after a while to conserve power.


# Drawbacks

RFC.

# Alternatives

RFC.

# Appended information

https://developer.apple.com/library/archive/qa/qa1931/_index.html

